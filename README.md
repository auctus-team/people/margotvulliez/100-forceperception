# Description

This chai3d application simulates different mass/stiffness/damping environements to be feedback through a haptic device.

The experiment randomly presents two different stimuli per trial to be compared by the user, each one started when the Enter key is pressed. The user indicates if the second stimulus is higher (right key) or not (left key).

A 2Hz back-and-forth x-motion is requested to simulate equivalent force environement in all condition. Some haptic guidance and a tempo sound are additionally feedback to help the user.

The experiment aims at identifying the human perceptive model (JND, slope, bias...), either through a constant or psychophysic method.

# Application configuration

The experiment can be configured in the code `chai3d/examples/GLFW/100-forceperception.cpp`, by setting the following parameters:

- `int method`: select the analysis method as `CONSTANT` or `PSY` (psychophysic)
- `int property`: select the tested type of stimulus among `INERITA`, `STIFFNESS`, or `DAMPING`
- `cVector3d desiredPosition`: set the spring position in the device frame to simulate the stiffness environement
- `double refStiffness`: set the reference stiffness value (in N/m)
- `double refDamping`: set the reference damping value (in N/m/s)
- `double refMass`: set the reference mass value (in kg)
- `const int numberStimulus`: number of different tested stimulus values in the constant method
- `array<double, numberStimulus> listJND`: list of tested stimulus values in the constant method, in % of the reference value
- `const int numberTrial`: number of trials per stimulus value in the constant method
- `string filenameJND`: path and name of the csv file to record experimental results to compute the JND = presented stimulus (value and % of the reference) and user answer
- `string filenameData`: path and name of the csv file to record experimental data from the device (force and position)

# Install & Build

The code must be cloned in the chai3d example folder.

```console
cd chai3d/examples/GLFW
git clone https://gitlab.inria.fr/auctus-team/people/margotvulliez/100-forceperception.git
```

Add `100-forceperception` to the app list in the example cmake files:
`chai3d/examples/GLFW/CMakeList.txt` && `Makefile`

Then build the code

```console
cd chai3d/build
cmake .. && make -j8 
```

# Use

Examples are run from the bin folder, in sudo mode to detect the plugged haptic device

```console
cd chai3d/bin/lin-x86_64
sudo ./100-forceperception.cpp
```


