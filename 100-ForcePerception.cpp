//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D.
    \version   3.2.0 $Rev: 2051 $
    (www.chai3d.org)

    \author    Margot Vulliez

    This chai3d application simulates different mass/stiffness/damping [int property] environements to be feedback through a haptic device.
    The experiment presents different stimuli to be compared by the user.
    It aims at identifying the human perceptive model (JND, slope, bias...), either through a constant or psychophysic method [int method].
*/
//==============================================================================

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include "chai3d.h"
#include <iostream>
#include <fstream>
#include <string>
#include <GLFW/glfw3.h>
//------------------------------------------------------------------------------
#include "MatlabEngine.hpp"
#include "MatlabDataArray.hpp"
//------------------------------------------------------------------------------
using namespace chai3d;
using namespace std;

//------------------------------------------------------------------------------
// DECLARED VARIABLES
//------------------------------------------------------------------------------

// Scene settings
//------------------------------------------------------------------------------
// fullscreen mode
bool fullscreen = false;
// mirrored display
bool mirroredDisplay = false;
// a world that contains all objects of the virtual environment
cWorld* world;
// a camera to render the world in the window display
cCamera* camera;
// a light source to illuminate the objects in the world
cDirectionalLight *light;
// a few shape primitives that compose our scene
cShapeSphere* sphere0;
cShapeSphere* sphere1;
cShapeLine* line;
// a font for rendering text
cFontPtr font;
// a label to display the position [m] of the haptic device
cLabel* labelHapticDevice;
// a small scope to display the interaction force signal
cScope* scope;
// a handle to window display context
GLFWwindow* window = NULL;
// current width of window
int width  = 0;
// current height of window
int height = 0;
// swap interval for the display context (vertical synchronization)
int swapInterval = 1;

// Haptic device settings
//------------------------------------------------------------------------------
// a haptic device handler
cHapticDeviceHandler* handler;
// a pointer to the current haptic device
cGenericHapticDevicePtr hapticDevice;
// device properties
double maxStiffness;
double maxDamping;
double maxLinearForce;
// a small sphere (cursor) representing the haptic device 
cShapeSphere* cursor;
// Scaling factor from haptic motion to virtual environement
double scaling;

// Haptic device variables
//------------------------------------------------------------------------------
// position [m] of the haptic device
cVector3d position;
// device home position [m]
cVector3d homePosition;
// velocity [m/s] of the haptic device
cVector3d linearVelocity;
// acceleration [m/s²] of the haptic device
cVector3d linearAcceleration;
cVector3d filtredLinearAcceleration;      
// butterworth filter variables
Eigen::MatrixXd rawBuffer;
Eigen::MatrixXd filteredBuffer;
Eigen::VectorXd rawCoeff;
Eigen::VectorXd filteredCoeff;
double normalizedCutOff;
double preWrapCutOff;
// feedback force [N] of the haptic device
cVector3d force;
// a global variable to switch from homing to teleoperation
bool button;
// a global variable to indicate perceived difference in the stimulus
bool isLargerUser;
// detect is the decision key was pressed
bool isPressed;

// Haptic guidance parameters
//------------------------------------------------------------------------------
// guiding radius [m]
double radius;
// amplitude motion stoppers [m]
double distanceStopper;
// guiding force [N]
cVector3d guideForce;
// guiding direction unit vector
cVector3d guideVector;
// distance to guide vector
cVector3d distanceVector;
// guidance stiffness [N/m]
double Kp;
// guidance damping [N/m/s]
double Kv;

// Experimental conditions
//------------------------------------------------------------------------------
//Definition of the environement property profile
#define INERTIA      0
#define STIFFNESS	 1
#define DAMPING		 2
int property;
//Definition of the stimulus method
#define PSY          0
#define CONSTANT     1
int method;
//Definiton of the device control mode
#define HOMING          0
#define TELEOPERATION   1
int state;

// Experimental parameters
//------------------------------------------------------------------------------
// desired position for the stiffness stimulus
cVector3d desiredPosition;
// reference & actual stiffness [N/m]
double stiffness;
double refStiffness;
//  reference & actual damping [N/m/s]
double damping;
double refDamping;
//  reference & actual mass [kg]
double mass;
double refMass;
// number of trials per stimulus
const int numberTrial = 15;
// number of different stimulus values
const int numberStimulus = 8;
// List of % stimulus from the reference for the constant method
array<double, numberStimulus> listJND;
// List of number of trials for each stimulus
array<int, numberStimulus> listTrial;
// selected index in the list
int indexStimulus;
// Value of JND applied to the modified stimulus for the pshyco method
double currentJND;
// counter of motion cycles for each presented stimulus
int counter;
// counter to ensure that two stimuli are presented
int countStimuli;
// boolean to either select the reference stimulus or the modified stimulus
bool modifiedStimulus;
// a global variable to indicate actual difference in the stimulus
bool isLarger;

// Variables to record/export the data
//------------------------------------------------------------------------------
// create an ofstream for the file output
ofstream outputFile;
// create a name for the file output
string filenameJND = "/home/mvulliez/Documents/DataPerception/jnd01StiffnessConstant.csv";
string filenameData = "/home/mvulliez/Documents/DataPerception/data01StiffnessConstant.csv";
// total number of presented stimulus
const int totalStimulus = numberTrial * numberStimulus;
// list of presented JND
array<double,totalStimulus> presentedJND;
// list of presented stimulus
array<double,totalStimulus> presentedStimulus;
// list of answer
array<int,totalStimulus> isCorrect;
// index for data recording
int k = 0;
// recorded device position
vector<double> devicePositionX;
vector<double> devicePositionY;
vector<double> devicePositionZ;
// recorded device force
vector<double> deviceForceX;
vector<double> deviceForceY;
vector<double> deviceForceZ;
// global recorded data
vector<pair<string,vector<double>>> data;

// Application operating variables
//------------------------------------------------------------------------------
// a flag to indicate if the haptic simulation currently running
bool simulationRunning = false;
// a flag to indicate if the haptic simulation has terminated
bool simulationFinished = true;
// a frequency counter to measure the simulation graphic rate
cFrequencyCounter freqCounterGraphics;
// a frequency counter to measure the simulation haptic rate
cFrequencyCounter freqCounterHaptics;
// haptic thread
cThread* hapticsThread;
// matlab thread
cThread* matlabThread;





//------------------------------------------------------------------------------
// DECLARED FUNCTIONS
//------------------------------------------------------------------------------

// Scene managing functions
//------------------------------------------------------------------------------
// callback when the window display is resized
void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height);
// callback when an error GLFW occurs
void errorCallback(int error, const char* a_description);
// callback when a key is pressed
void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods);

// Visual and haptic feedback rendering
//------------------------------------------------------------------------------
// this function renders the scene
void updateGraphics(void);
// this function contains the main haptics simulation loop
void updateHaptics(void);
// this function closes the application
void close(void);

// Tool functions
//------------------------------------------------------------------------------
// A filtering function to compute the acceleration
cVector3d butterworthFilter(cVector3d& unfiltredVector);

// Matlab export/import function
//------------------------------------------------------------------------------
// Calling Matlab function
void callMatlab();




//==============================================================================
/*
    DEMO:   100-ForcePerception.cpp

*/
//==============================================================================

int main(int argc, char* argv[])
{
    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << "CHAI3D" << endl;
    cout << "Demo: 100-ForcePerception" << endl;
    cout << "-----------------------------------" << endl << endl << endl;
    cout << "Keyboard Options:" << endl << endl;
    cout << "[f] - Enable/Disable full screen mode" << endl;
    cout << "[m] - Enable/Disable vertical mirroring" << endl;
    cout << "[q] - Exit application" << endl;
    cout << endl << endl;
    
    //--------------------------------------------------------------------------
    // INITIALIZATION
    //--------------------------------------------------------------------------
    
    // Configure the experimental parameters
    //------------------------------------------------------------------------------
    // select the analysis method (CONSTANT or PSY)
    method = CONSTANT;
    // select the stimulus type (INERTIA or STIFFNESS or DAMPING)
    property = STIFFNESS;
    // desired position stiffness stimulus
    desiredPosition.set(0.0, 0.0, 0.0);
    //desired stiffness [N/m]
    refStiffness = 80;
    stiffness = refStiffness;
    // desired damping [N/m/s]
    refDamping = 12.8;
    damping = refDamping;
    // desired mass [kg]
    refMass = 2;
    mass = refMass;
    // init the list of tested % stimulus from the reference;
    listJND = {-100.0, -31.0, -10.0, -3.0, 3.0, 10.0, 31.0, 100.0};
    // init the list of remaining number of trials for each stimulus
    for(int p = 0; p<numberStimulus; p++)
    {
        listTrial[p] = numberTrial;
    }
    // init first tested stimulus
    switch (method)
    {
        case PSY:
            currentJND = 10; // TO READ FROM MATLAB
        break;
        case CONSTANT:
            indexStimulus = rand() % 8;
            currentJND = listJND[indexStimulus];
            listTrial[indexStimulus] = listTrial[indexStimulus]-1;
        break;
    }               
    // init stimuli counter
    countStimuli = 1;

    // Initialize the haptic parameters
    //------------------------------------------------------------------------------
    // initial control mode
    state = HOMING;
    // home device position 
    homePosition.set(0.0, 0.0, 0.0);
    // init user switch
    button = false;
    // init user decision is not given
    isPressed = false;
    // init counter
    counter=0;
    // init zero feedback force [N]
    force.set(0.0,0.0,0.0);

    // Initialize the haptic guidance
    //------------------------------------------------------------------------------
    // guiding radius [m]
    radius = 0.015;
    // radius = 0.005;
    // distance from center to stopper [m]
    distanceStopper = 0.045;
    // distanceStopper = 0.04;
    // guiding direction unit vector
    guideVector.set(1.0, 0.0, 0.0);
    // guidance stiffness [N/m]
    Kp = 1500;
    // guidance damping [N/m/s]
    Kv = 10;
    // scaling factor
    scaling = 10.0;

    // Initialize the filter variables
    //------------------------------------------------------------------------------
    normalizedCutOff = 0.0005;
    rawBuffer.setZero(3, 3);
    filteredBuffer.setZero(3, 2);
    rawCoeff.setZero(3);
    filteredCoeff.setZero(2);
    preWrapCutOff = tan(M_PI*normalizedCutOff);
    double gain = (1/(preWrapCutOff*preWrapCutOff) + sqrt(2)/preWrapCutOff + 1);
    rawCoeff << 1, 2, 1;
    rawCoeff /= gain;
    filteredCoeff << 2-2/(preWrapCutOff*preWrapCutOff), 1/(preWrapCutOff*preWrapCutOff) - sqrt(2)/preWrapCutOff + 1;
    filteredCoeff /= gain;



    //--------------------------------------------------------------------------
    // OPEN GL - WINDOW DISPLAY
    //--------------------------------------------------------------------------
    // initialize GLFW library
    if (!glfwInit())
    {
        cout << "failed initialization" << endl;
        cSleepMs(1000);
        return 1;
    }
    // set error callback
    glfwSetErrorCallback(errorCallback);
    // compute desired size of window
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    int w = 0.8 * mode->height;
    int h = 0.5 * mode->height;
    int x = 0.5 * (mode->width - w);
    int y = 0.5 * (mode->height - h);
    // set OpenGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    // set active stereo mode
    glfwWindowHint(GLFW_STEREO, GL_FALSE);
    // create display context
    window = glfwCreateWindow(w, h, "CHAI3D", NULL, NULL);
    if (!window)
    {
        cout << "failed to create window" << endl;
        cSleepMs(1000);
        glfwTerminate();
        return 1;
    }
    // get width and height of window
    glfwGetWindowSize(window, &width, &height);
    // set position of window
    glfwSetWindowPos(window, x, y);
    // set key callback
    glfwSetKeyCallback(window, keyCallback);
    // set resize callback
    glfwSetWindowSizeCallback(window, windowSizeCallback);
    // set current display context
    glfwMakeContextCurrent(window);
    // sets the swap interval for the current display context
    glfwSwapInterval(swapInterval);
    #ifdef GLEW_VERSION
        // initialize GLEW library
        if (glewInit() != GLEW_OK)
        {
            cout << "failed to initialize GLEW library" << endl;
            glfwTerminate();
            return 1;
        }
    #endif


    //--------------------------------------------------------------------------
    // WORLD - CAMERA - LIGHTING
    //--------------------------------------------------------------------------
   
    // Set the world global view 
    //--------------------------------------------------------------------------
    // create a new world.
    world = new cWorld();
    // set the background color of the environment
    world->m_backgroundColor.setBlack();
    // create a camera and insert it into the virtual world
    camera = new cCamera(world);
    world->addChild(camera);
    // position and orient the camera
    camera->set( cVector3d(3.0, 0.0, 0.0),    // camera position (eye)
                 cVector3d(0.0, 0.0, 0.0),    // lookat position (target)
                 cVector3d(0.0, 0.0, 1.0));   // direction of the (up) vector
    // set the near and far clipping planes of the camera
    camera->setClippingPlanes(0.01, 10.0);
    // set stereo mode
    camera->setStereoMode(C_STEREO_DISABLED);
    // set stereo eye separation and focal length (applies only if stereo is enabled)
    camera->setStereoEyeSeparation(0.02);
    camera->setStereoFocalLength(3.0);
    // set vertical mirrored display mode
    camera->setMirrorVertical(mirroredDisplay);
    // create a directional light source
    light = new cDirectionalLight(world);
    // insert light source inside world
    world->addChild(light);
    // enable light source
    light->setEnabled(true);
    // define direction of light beam
    light->setDir(-1.0, 0.0, 0.0);

    // Set the scene components
    //--------------------------------------------------------------------------
    // create a sphere (cursor) to represent the haptic device
    cursor = new cShapeSphere(0.05);
    // insert cursor inside world
    world->addChild(cursor);
    // create and insert two spheres to represent the motion limits
    sphere0 = new cShapeSphere(0.03);
    world->addChild(sphere0);
    sphere1 = new cShapeSphere(0.03);
    world->addChild(sphere1);
    // set position of the limitation spheres
    sphere0->setLocalPos(0.0,0.0, -0.4);
    sphere1->setLocalPos(0.0, 0.0, 0.4);
    // set material color
    sphere0->m_material->setRedFireBrick();
    sphere1->m_material->setRedFireBrick();
    // create a line to represent the path to follow
    line = new cShapeLine(sphere0, sphere1);
    world->addChild(line);
    // set color at each point
    line->m_colorPointA.setWhite();
    line->m_colorPointB.setWhite();
    // create a font
    font = NEW_CFONTCALIBRI20();
    // create a label to display the position and force of the haptic device
    labelHapticDevice = new cLabel(font);
    camera->m_frontLayer->addChild(labelHapticDevice);
    // set position of the label
    labelHapticDevice->setLocalPos(20, height - 60, 0);
    // create a scope to plot the haptic device force
    scope = new cScope();
    camera->m_frontLayer->addChild(scope);
    scope->setSize(400, 200);
    scope->setRange(-2.0, 2.0);
    scope->setSignalEnabled(true, false, false, false);
    scope->setShowPanel(false);
    scope->m_colorSignal0.setRedCrimson();




    //--------------------------------------------------------------------------
    // HAPTIC DEVICE
    //--------------------------------------------------------------------------
    // create a haptic device handler
    handler = new cHapticDeviceHandler();
    // get access to the first available haptic device found
    handler->getDevice(hapticDevice, 0); 
    // open a connection to haptic device
    hapticDevice->open();
    // calibrate device (if necessary)
    hapticDevice->calibrate();
    // retrieve information about the current haptic device
    cHapticDeviceInfo hapticDeviceInfo = hapticDevice->getSpecifications();
    // if the device has a gripper, enable the gripper to simulate a user switch
    hapticDevice->setEnableGripperUserSwitch(true);
    // get properties of haptic device
    maxStiffness	= hapticDeviceInfo.m_maxLinearStiffness;
    maxDamping = hapticDeviceInfo.m_maxLinearDamping;
    maxLinearForce = hapticDeviceInfo.m_maxLinearForce;




    //--------------------------------------------------------------------------
    // SIMULATION
    //--------------------------------------------------------------------------
    
    // Matlab communication loop
    //--------------------------------------------------------------------------
    // create a thread which starts the matlab communication loop
    matlabThread = new cThread();
    //matlabThread->start(callMatlab, CTHREAD_PRIORITY_HAPTICS); //TODO

    // Main haptic loop
    //--------------------------------------------------------------------------
    // create a thread which starts the main haptics rendering loop
    hapticsThread = new cThread();
    hapticsThread->start(updateHaptics, CTHREAD_PRIORITY_HAPTICS);

    // Main graphic loop
    //--------------------------------------------------------------------------
    // call window size callback at initialization
    windowSizeCallback(window, width, height);
    while (!glfwWindowShouldClose(window))
    {
        // get width and height of window
        glfwGetWindowSize(window, &width, &height);
        // render graphics
        updateGraphics();
        // swap buffers
        glfwSwapBuffers(window);
        // process events
        glfwPollEvents();
        // signal frequency counter
        freqCounterGraphics.signal(1);
    }

    // Stop simulation
    //--------------------------------------------------------------------------
    // setup callback when application exits
    atexit(close);
    // close window
    glfwDestroyWindow(window);
    // terminate GLFW library
    glfwTerminate();
    // exit
    return (0);
}





//--------------------------------------------------------------------------
// FUNCTIONS DEFINITION
//--------------------------------------------------------------------------
    
//------------------------------------------------------------------------------
void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height)
{
    // update window size
    width  = a_width;
    height = a_height;
    // update position of scope
    scope->setLocalPos((0.5 * (width - scope->getWidth())), 10);
}

//------------------------------------------------------------------------------
void errorCallback(int a_error, const char* a_description)
{
    cout << "Error: " << a_description << endl;
}

//------------------------------------------------------------------------------
void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
    // filter calls that only include a key press
    if ((a_action != GLFW_PRESS) && (a_action != GLFW_REPEAT))
    {
        return;
    }
    // option - exit
    else if ((a_key == GLFW_KEY_ESCAPE) || (a_key == GLFW_KEY_Q))
    {
        glfwSetWindowShouldClose(a_window, GLFW_TRUE);
    }
    // option - toggle fullscreen
    else if (a_key == GLFW_KEY_F)
    {
        // toggle state variable
        fullscreen = !fullscreen;

        // get handle to monitor
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();

        // get information about monitor
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);

        // set fullscreen or window mode
        if (fullscreen)
        {
            glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
            glfwSwapInterval(swapInterval);
        }
        else
        {
            int w = 0.8 * mode->height;
            int h = 0.5 * mode->height;
            int x = 0.5 * (mode->width - w);
            int y = 0.5 * (mode->height - h);
            glfwSetWindowMonitor(window, NULL, x, y, w, h, mode->refreshRate);
            glfwSwapInterval(swapInterval);
        }
    }
    // option - toggle vertical mirroring
    else if (a_key == GLFW_KEY_M)
    {
        mirroredDisplay = !mirroredDisplay;
        camera->setMirrorVertical(mirroredDisplay);
    }
     // option - switch to teleoperation
    else if (a_key == GLFW_KEY_ENTER)
    {
        button = true;
    }
    // option - decision on the stimulus
    else if (a_key == GLFW_KEY_RIGHT)
    {
    isLargerUser = true;
    isPressed ++;
    }
    else if (a_key == GLFW_KEY_LEFT)
    {
    isLargerUser = false;
    isPressed ++;
    }
}

//------------------------------------------------------------------------------
void close(void)
{
    // stop the simulation
    simulationRunning = false;
    // wait for graphics and haptics loops to terminate
    while (!simulationFinished) { cSleepMs(100); }
    // close haptic device
    hapticDevice->close();
    // delete resources
    delete hapticsThread;
    delete matlabThread;
    delete world;
    delete handler;
}

//------------------------------------------------------------------------------
cVector3d butterworthFilter(cVector3d& unfiltredVector)
{
	for(int i = 2; i>0; i--)
	{
		rawBuffer.col(i) = rawBuffer.col(i-1);
	}
	rawBuffer.col(0) << unfiltredVector(0), unfiltredVector(1), unfiltredVector(2);

	Eigen::VectorXd y = rawBuffer*rawCoeff - filteredBuffer*filteredCoeff;

	for(int i = 1; i>0; i--)
	{
		filteredBuffer.col(i) = filteredBuffer.col(i-1);
	}
	filteredBuffer.col(0) = y;

    cVector3d filtredVector;
    filtredVector.set(y(0), y(1), y(2));
	return filtredVector;
}





//------------------------------------------------------------------------------
void updateGraphics(void)
{
    // update position and force data in the label
    labelHapticDevice->setText("position " + position.str(3)+ " m / " + "force "+force.str(3));
    // update force data in the scope
    scope->setSignalValues( force.length() );

    // user answer and computation of new experimental parameters //
    if (countStimuli==3) // wait for testing the two stimuli
    {
        // wait for the user answer
        if (isPressed)
        {
            // record presented JND
            presentedJND[k] = currentJND;
            // record presented stimulus
            switch(property)
            {
                case INERTIA:
                    presentedStimulus[k] = refMass + currentJND*refMass/100;
                break;
                case DAMPING:
                    presentedStimulus[k] = refDamping + currentJND*refDamping/100;
                break;
                case STIFFNESS:
                    presentedStimulus[k] = refStiffness + currentJND*refStiffness/100;
                break;
            }
            // record user answer
            if (isLarger==isLargerUser)
            {
                isCorrect[k] = 1;
                cout << "correct" << endl;
            }
            else 
            {
                isCorrect[k] = 0;
                cout << "error" << endl;
            }
            // compute new stimulus depending on the analysis method
            switch(method)
            {
                case PSY: // psychophysic method
                    // wait for the experimentator to enter new JND from Matlab
                    cout << "entrer le nouveau JND ou terminer en tapant 999" << endl;
                    cin >> currentJND;
                    cout << "new JND is:" << currentJND << endl;
                    // check if the experiment is finished
                    if (currentJND==999)
                    {
                        cout << "Fin de l'expérimentation" << endl;
                        // record data to a .csv file
                        outputFile.open(filenameJND);
                        // write the file headers
                        outputFile << "Stimulus" << ", " << "JND" << ", " << "isCorrect" << endl;
                        // write the data to the output file
                        for (int n = 0; n < (k+1); n++)
                        {
                            outputFile << presentedStimulus[n] << ", " << presentedJND[n] << ", " << isCorrect[n] << endl;    
                        }
                        // close the output file
                        outputFile.close();
                        // export data to a .csv file
                        outputFile.open(filenameData);
                        // write the file headers
                        for(int j = 0; j < data.size(); j++)
                        {
                            outputFile << data.at(j).first;
                            if(j != data.size() - 1) outputFile << ","; // No comma at end of line
                        }
                        outputFile << "\n";
                        // write the data to the output file
                        for(int i = 0; i < data.at(0).second.size(); i++)
                        {
                            for(int j = 0; j < data.size(); j++)
                            {
                                outputFile << data.at(j).second.at(i);
                                if(j != data.size() - 1) outputFile << ","; // No comma at end of line
                            }
                            outputFile << "\n";
                        }
                        outputFile.close();
                        // close application and device
                        close();
                    }
                    // restart new trials
                    countStimuli=1;
                break;
                case CONSTANT: // constant method
                    // check if the experiment is finished
                    int cond = 0;
                    for(int p = 0; p<numberStimulus; p++)
                    {
                        cond += listTrial[p];
                    }
                    if (cond == 0)
                    {
                        cout << "Fin de l'expérimentation" << endl;
                        // export JND results to a .csv file
                        outputFile.open(filenameJND);
                        // write the file headers
                        outputFile << "Stimulus" << ", " << "JND" << ", " << "isCorrect" << endl;
                        // write the data to the output file
                        for (int n = 0; n < (k+1); n++)
                        {
                            outputFile << presentedStimulus[n] << ", " << presentedJND[n] << ", " << isCorrect[n] << endl;    
                        }
                        // close the output file
                        outputFile.close();
                        // export data to a .csv file
                        outputFile.open(filenameData);
                        // write the file headers
                        for(int j = 0; j < data.size(); j++)
                        {
                            outputFile << data.at(j).first;
                            if(j != data.size() - 1) outputFile << ","; // No comma at end of line
                        }
                        outputFile << "\n";
                        // write the data to the output file
                        for(int i = 0; i < data.at(0).second.size(); ++i)
                        {
                            for(int j = 0; j < data.size(); ++j)
                            {
                                outputFile << data.at(j).second.at(i);
                                if(j != data.size() - 1) outputFile << ","; // No comma at end of line
                            }
                            outputFile << "\n";
                        }
                        outputFile.close();
                        // close application and device
                        close();
                    }
                    else
                    {
                        // pick-up a random new stimulus from the list
                        indexStimulus = rand() % numberStimulus;
                        // redo if the stimulus has already been tested 
                        while (listTrial[indexStimulus]==0)
                        {
                            indexStimulus = rand() % numberStimulus;
                        }
                        currentJND = listJND[indexStimulus];
                        listTrial[indexStimulus] = listTrial[indexStimulus]-1;
                        cout << "new JND is:" << currentJND << endl;
                        // restart new trials
                        countStimuli=1;
                    }
                break;
            }
            k++;
        }
    }
    // update scene rendering //
    // shadow maps (if any)
    world->updateShadowMaps(false, mirroredDisplay);
    // render world
    camera->renderView(width, height);
    // wait until all GL commands are completed
    glFinish();
    // check for any OpenGL errors
    GLenum err;
    err = glGetError();
    if (err != GL_NO_ERROR) cout << "Error:  %s\n" << gluErrorString(err);
}

//------------------------------------------------------------------------------
void updateHaptics(void)
{
    // local intialization before simulation starts //
    // precision clock to compute the device acceleration
    cPrecisionClock clock;
    clock.reset();
    // precision clock to generate a bipper sound
    cPrecisionClock clock_bip;
    clock_bip.reset();
    // velocity memory
    cVector3d previouslinearVelocity;
    previouslinearVelocity.set(0.0, 0.0, 0.0);
    // simulation in now running
    simulationRunning  = true;
    simulationFinished = false;
    // start clocks
    clock_bip.start();
    clock.start();
    double timeInterval = 0.0;
    double previousTime = 0.0;
    double initTime = 0.0;
    int m;
    // main haptic simulation loop //
    while(simulationRunning)
    {
        // read the time increment in seconds
        timeInterval = clock.getCurrentTimeSeconds() - previousTime;
        previousTime = clock.getCurrentTimeSeconds();
        // signal frequency counter
        freqCounterHaptics.signal(1);

        // read haptic device infos //
        // read position 
        hapticDevice->getPosition(position);
        // read linear velocity 
        hapticDevice->getLinearVelocity(linearVelocity);
        //compute linear acceleration
        linearAcceleration = (linearVelocity-previouslinearVelocity)/timeInterval;
        previouslinearVelocity = linearVelocity;
        filtredLinearAcceleration = butterworthFilter(linearAcceleration);
        // read user-switch status
        //hapticDevice->getUserSwitch(0, button);

        // update scene cursor from the haptic device //
        // compute global reference frames for each scene object
        world->computeGlobalPositions(true);
        // update position and orientation of cursor
        cursor->setLocalPos(scaling*position(2),scaling*position(1),-scaling*position(0));

        // haptic force computation //
        switch(state)
        {
            // maintain the device to its home position between two trials
            case HOMING: 
                // evaluate position controller force
                force = -0.1 * maxStiffness*(position - homePosition) - 0.5 * maxDamping * linearVelocity;
                // if enter is pressed, switch to teleoperation mode
                if (button && countStimuli<=2)
                {   
                    // select stimulus to be presented
                    if (countStimuli==1)
                    {
                        // randomly select the first presented stimulus (reference or modified one) 
                        modifiedStimulus = rand() % 2;
                        // consequent expected answer to the comparison test
                        if (modifiedStimulus)
                        {
                            if (currentJND >= 0)
                            {
                                isLarger=false;
                            }
                            else
                            {
                                isLarger=true;
                            }
                        }
                        else
                        {
                            if (currentJND >= 0)
                            {
                                isLarger=true;
                            }
                            else
                            {
                                isLarger=false;
                            }
                        }
                        countStimuli ++;
                    }
                    else if (countStimuli==2)
                    {
                        modifiedStimulus = !modifiedStimulus;
                        countStimuli ++;
                    }
                    // reset counter and start button to zero
                    button = false;
                    counter = 0;
                    m = 0;
                    // read current time
                    initTime = clock.getCurrentTimeSeconds();
                    // stop, reset and start the bipper clock
                    clock_bip.stop();
                    clock_bip.reset();
                    clock_bip.start();
                    state = TELEOPERATION;
                }
            break;
            case TELEOPERATION:
                // start an audio bip at 2Hz
                if(clock_bip.getCurrentTimeSeconds()>=0.5)
                {
                    cout << '\a' << endl;
                    counter ++;
                    // stop, reset and start the bipper clock
                    clock_bip.stop();
                    clock_bip.reset();
                    clock_bip.start();
                }
                // compute current stimulus value (reference or modified stimulus)
                if (modifiedStimulus)
                {
                    mass = refMass + currentJND*refMass/100;
                    stiffness = refStiffness + currentJND*refStiffness/100;
                    damping = refDamping + currentJND*refDamping/100;
                }
                else
                {
                    mass = refMass;
                    stiffness = refStiffness;
                    damping = refDamping;
                }
                // compute force feedback for the chosen stimulus property
                switch(property)
                {
                    case INERTIA:
                        force = -mass * filtredLinearAcceleration;
                        break;
                    case STIFFNESS:
                        force = stiffness * (desiredPosition - position);
                        break;
                    case DAMPING:
                        force = -damping * linearVelocity;
                        break;
                    default:
                        force.set(0.0,0.0,0.0);
                }
                // compute guidance force corridor 
                distanceVector = position - (position.dot(guideVector))*guideVector;
                if (distanceVector.length() >= radius)
                {
                    guideForce = - Kp * (distanceVector.length() - radius)*distanceVector/distanceVector.length() - Kv * linearVelocity;
                }
                else 
                {
                    guideForce.set(0.0,0.0,0.0);
                }
                // compute guidance stoppers
                if ((position.dot(guideVector)) >= distanceStopper)
                {
                    guideForce +=   - Kp * ((position.dot(guideVector))-distanceStopper)*guideVector - Kv * linearVelocity;
                }
                else if ((position.dot(guideVector)) <= -distanceStopper)
                {
                    guideForce +=   - Kp * ((position.dot(guideVector))+distanceStopper)*guideVector - Kv * linearVelocity;
                }
                // add guidance forces to the stimulus feedback force
                force += guideForce;
                // record data at 100Hz
                if((clock.getCurrentTimeSeconds() - initTime)>=0.01)
                {
                    // record device position
                    devicePositionX.push_back(position(0));
                    devicePositionY.push_back(position(1));
                    devicePositionZ.push_back(position(2));
                    // record device force
                    deviceForceX.push_back(force(0));
                    deviceForceY.push_back(force(1));
                    deviceForceZ.push_back(force(2));
                    initTime = clock.getCurrentTimeSeconds();
                }
                // count for 5 back-and-forth motions before going back to the home position
                if (counter >=10)
                {
                    force.set(0.0,0.0,0.0);
                    counter = 0;
                    isPressed=false;
                    // record data
                    data.push_back({"Fx", deviceForceX});
                    data.push_back({"Fy", deviceForceY});
                    data.push_back({"Fz", deviceForceZ});
                    data.push_back({"X", devicePositionX});
                    data.push_back({"Y", devicePositionY});
                    data.push_back({"Z", devicePositionZ});
                    // clear recording vectors
                    devicePositionX.clear();
                    devicePositionY.clear();
                    devicePositionZ.clear();
                    deviceForceX.clear();
                    deviceForceY.clear();
                    deviceForceZ.clear();
                    // if the two stimulus have been presented wait for the user answer
                    if (countStimuli==3)
                    {
                        cout << "appuyer à droite si le second stimuli était plus raide, à gauche sinon" << endl;
                    }
                    state = HOMING;
                }
            break;
            default:
                force.set(0.0,0.0,0.0);
        }
        // saturate command to force limits of the haptic device
        if (force.length() > maxLinearForce)
        {
            force = maxLinearForce*force/(force.length());
        }
        // send computed force feedback to the haptic device
        hapticDevice->setForce(force);
    }
    // exit haptics thread when simulation stops
    simulationFinished = true;
}

//------------------------------------------------------------------------------ //TODO
void callMatlab()
{
    using namespace matlab::engine;

    // Start MATLAB engine synchronously
	std::unique_ptr<MATLABEngine> matlabPtr = startMATLAB();

	// //Create MATLAB data array factory
	// matlab::data::ArrayFactory factory;
	// // Define a four-element typed array
	// matlab::data::TypedArray<double> const argArray = factory.createArray({ 1,4 }, { -2.0, 2.0, 6.0, 8.0 });


	// // Call MATLAB sqrt function on the data array
	// matlab::data::Array const results = matlabPtr->feval(u"sqrt", argArray);
	// // Display results
	// for (int i = 0; i < results.getNumberOfElements(); i++) {
	// 	double a = argArray[i];
	// 	std::complex<double> v = results[i];
	// 	double realPart = v.real();
	// 	double imgPart = v.imag();
	// 	std::cout << "Square root of " << a << " is " <<
	// 		realPart << " + " << imgPart << "i" << std::endl;}

}